#ifndef MYSQLITE_H
#define MYSQLITE_H
#include "sqlite3.h"

SQLITE_API void my_sqlite3_init();
SQLITE_API int my_sqlite3_statement_columns(void* stmt,char** outString);
SQLITE_API int my_sqlite3_output_free(char* ptr);
SQLITE_API int my_sqlite3_sql_columns(void* db,const char* sql , char** outString);
SQLITE_API int my_sqlite3_open_default_v2(const char* dbfile,sqlite3** dbhandle ,int flags);
SQLITE_API sqlite3* my_sqlite3_open_default(const char* dbfile);
#endif // MYSQLITE_H
