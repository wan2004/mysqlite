#include <stdio.h>
#include <wchar.h>
#include <stdlib.h>
#include <string.h>
#include "mysqlite.h"

#define bufferLen 1024

SQLITE_API void my_sqlite3_init();


#ifdef SQLITE_ENABLE_MEMSYS5
static unsigned char dataHeap[1024*1024*10];
#endif
void my_sqlite3_init()
{
    int r;
    r = my_sqlite3_config(SQLITE_CONFIG_MULTITHREAD );
    if(r!=0)printf("config thread heap fail = %d\n",r);

#ifdef SQLITE_ENABLE_MEMSYS5
    r = my_sqlite3_config(SQLITE_CONFIG_HEAP,dataHeap,sizeof(dataHeap),32);
    if(r!=0)printf("config malloc heap fail = %d\n",r);
#endif

#ifdef SQLITE_OMIT_AUTOINIT
    r = my_sqlite3_initialize();
    if(r!=0)printf("sqlite3_initialize fail = %d\n",r);
#endif
}
char* my_sqlite3_statement_columns_v2(void* s)
{
    sqlite3_stmt* stmt =(sqlite3_stmt*)s;

    char buffer[bufferLen];
    memset(buffer,0,bufferLen);
    int count = my_sqlite3_column_count(stmt);
    int len = 0;
    int i;
    for( i= 0;i< count ;i++)
    {
        const char* t_name = my_sqlite3_column_name(stmt , i);
        if(t_name)
        {
            int tlen = strlen(t_name) ;
            len += tlen;
            if(len <= bufferLen)
            {
                strncat(buffer, "|",2);
                strncat(buffer, t_name,tlen);
            }else{
                len = len - tlen;
                break;
            }
        }
    }
    len++;
    char* output = malloc(len) ;

    strncpy(output,(buffer+1),len + 1);
    output[len-1] = '\0';
//    strcat(output,'\0');
    return output;
}

int my_sqlite3_statement_columns(void* s,char** outString)
{
    sqlite3_stmt* stmt =(sqlite3_stmt*)s;

    char buffer[bufferLen];
    memset(buffer,0,bufferLen);
    int count = my_sqlite3_column_count(stmt);
    int len = 0;
    int i;
    for( i= 0;i< count ;i++)
    {
        const char* t_name = my_sqlite3_column_name(stmt , i);
        if(t_name)
        {
            int tlen = strlen(t_name) ;
            len = len + tlen + 1;
            if(len >= bufferLen)
            {
                len = len - tlen - 1;
                break;
            }else{
                strncat(buffer, "|",1);

                strncat(buffer, t_name,tlen);
            }
        }
    }
    int mallocLen = len + 1;
    *outString = malloc(mallocLen);
//    memset(*outString,0,mallocLen);
    strncpy(*outString,(buffer+1),len );
    (*outString)[len] = '\0';
    return len;
}

int my_sqlite3_sql_columns(void* db,const char* sql , char** outString)
{
    sqlite3* dbptr = (sqlite3*)db;
    sqlite3_stmt* tempStmt;
    int rel = 0;
    rel = my_sqlite3_prepare_v2(dbptr, sql, -1, &tempStmt, 0);
    if(rel)printf("\n prepare op = %d \n" , rel );

    int len = my_sqlite3_statement_columns(tempStmt,outString);
    rel = my_sqlite3_finalize(tempStmt);
    if(rel)printf("\n sqlite3_finalize op = %d \n" , rel );

    tempStmt = 0;
    return len;
}

sqlite3*  my_sqlite3_open_default(const char* dbfile)
{
    sqlite3* dbHandle = 0;
    int op = my_sqlite3_open_v2(dbfile, &dbHandle, SQLITE_OPEN_CREATE | SQLITE_OPEN_READWRITE | SQLITE_OPEN_SHAREDCACHE, 0);
    if(op) printf(" ### open fail database = %s return value = %d" , dbfile, op);
    return dbHandle;
}

int  my_sqlite3_open_default_v2(const char* dbfile,sqlite3** dbhandle ,int flags)
{
    sqlite3* dbHandle = 0;
    if(flags < 0)
        flags = SQLITE_OPEN_CREATE | SQLITE_OPEN_READWRITE | SQLITE_OPEN_SHAREDCACHE;

    int op = my_sqlite3_open_v2(dbfile, &dbHandle, flags , 0);
    if(op) printf(" ### open fail database = %s return value = %d" , dbfile, op);
    *dbhandle = dbHandle;
    return op;
}

int my_sqlite3_output_free(char *ptr)
{
    if(ptr!= 0)
    {
        free(ptr);
        return 0;
    }else
        return 1;
}
