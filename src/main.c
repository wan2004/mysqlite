#include <stdio.h>
#include "mysqlite.h"
#include <openssl/aes.h>
#include "rijndael.h"
#include <string.h>

#define MODMULT1(a, b, c, m, s) q = s / a; s = b * (s - a * q) - c * q; if (s < 0) s += m

unsigned char* userkey = "test_password";
void TestKey(sqlite3* db)
{


    int rel = my_sqlite3_rekey(db,userkey,21);
    if(rel!=0)
        printf("\n rekey fail \n");
}

void
MyInitialVector( int seed, unsigned char iv[16])
{
  unsigned char initkey[16];
  int j, q;
  int z = seed + 1;
  for (j = 0; j < 4; j++)
  {
    MODMULT1(52774, 40692,  3791, 2147483399L, z);
    initkey[4*j+0] = 0xff &  z;
    initkey[4*j+1] = 0xff & (z >>  8);
    initkey[4*j+2] = 0xff & (z >> 16);
    initkey[4*j+3] = 0xff & (z >> 24);
  }
  memcpy(iv,initkey,16);
//  CodecGetMD5Binary(codec, (unsigned char*) initkey, 16, iv);
}

void TestAES()
{
    AES_KEY m_openssl_EnKey,m_openssl_DeKey;
    int uKeyLenInBytes = 16;
    unsigned char iv[16];
    unsigned char iv2[16];
    unsigned char data[] = "I am is not encrypt data,ready ";
    unsigned char* datain = data;
//    unsigned char* output = (unsigned char*)malloc(32);
    unsigned char outputBuf[32];
    unsigned char* output = outputBuf;
    unsigned char dataoutBuf[32];
    unsigned char* dataout = dataoutBuf;
    memset(output,0,32);

    AES_set_encrypt_key(userkey,uKeyLenInBytes * 8,&m_openssl_EnKey);



    MyInitialVector(1,iv);
    MyInitialVector(1,iv2);

    printf(" data = %s\n" , data );

    int i;
    for(i=0;i<=1;i++)
    {
        AES_cbc_encrypt(datain ,output,16, &m_openssl_EnKey,iv,AES_ENCRYPT );
        datain += 16;
        output += 16;
    }

    datain = data;
    output = outputBuf;

    printf(" outputBuf = %s\n" , outputBuf );
//    printf(" str size = %d " , strlen(outputBuf));

    AES_set_decrypt_key(userkey,uKeyLenInBytes * 8,&m_openssl_DeKey);

    for(i=0;i<=1;i++)
    {
        AES_cbc_encrypt(output,dataout,16, &m_openssl_DeKey,iv2,AES_DECRYPT );
        dataout += 16;
        output += 16;
    }
    printf(" dataoutBuf = %s\n" , dataoutBuf );
//    printf(" str size = %d " , strlen(outputBuf));

}

int main(void)
{
//    TestAES();

//    if(1)return ;

#ifdef SQLITE_OMIT_AUTOINIT
    my_sqlite3_init();
#endif

    char* error_output ;
    //memset(error_output,0,2048);
    printf("Hello World!\n");
    sqlite3* db;
    sqlite3_stmt* stmt;

    int rel =0 ;
    rel = my_sqlite3_open("1.db",&db);
    printf("op = %d\n",rel);

    rel = my_sqlite3_key(db,userkey,21);
    printf("op = %d\n",rel);


    rel = my_sqlite3_exec(db,"CREATE TABLE abc (col1 INTEGER ,col2 VARCHAR(50));",0,0,&error_output);
    printf("op = %d output = \n%s \n",rel,error_output);
    my_sqlite3_free(error_output);
    rel = my_sqlite3_prepare_v2(db,"INSERT INTO abc VALUES(5,'1234');",-1,&stmt,0);
    error_output = my_sqlite3_errmsg(db);
    printf("op = %d output = \n%s \n",rel,error_output);
    my_sqlite3_finalize(stmt);
    char* ctr = 0;
    int colstrLen =0;
    for(int i=0;i<100;i++){
        rel = my_sqlite3_prepare_v2(db,"select 1 as cc,2 as b2c,3 as CBBC,4 as c9999,5 as CBC",-1,&stmt,0);
        error_output = my_sqlite3_errmsg(db);
        printf("op = %d output = \n%s \n",rel,error_output);
//      my_sqlite3_free(error_output);
//      error_output = 0；
        colstrLen = my_sqlite3_statement_columns(stmt,&ctr);

        printf("column name = %s  len = %d \n" ,ctr ,colstrLen);
        my_sqlite3_output_free(ctr);
        my_sqlite3_finalize(stmt);
        if(i % 5 ==0)
        {

            sqlite3_int64 used = my_sqlite3_memory_used();
            printf("##memory use = %d  \n",used);

        }

        rel = my_sqlite3_prepare_v2(db,"select * from abc",-1,&stmt,&error_output);
        printf("op = %d output = \n%s \n",rel,error_output);
//        my_sqlite3_free(error_output);
//        error_output = 0;
        colstrLen = my_sqlite3_statement_columns(stmt,&ctr);
        printf("column name = %s  len = %d \n" ,ctr ,colstrLen);
        my_sqlite3_output_free(ctr);
        my_sqlite3_finalize(stmt);
    }
    TestKey(db);



    my_sqlite3_close_v2(db);

    int releaseNum = 0;
    releaseNum = my_sqlite3_release_memory(100000);
    releaseNum += my_sqlite3_db_release_memory(db);
    sqlite3_int64 highwater = my_sqlite3_memory_highwater(0);
    sqlite3_int64 used = my_sqlite3_memory_used();
    printf("## last  memory use = %d  highwater =%d release_memory =%d \n",used , highwater,releaseNum);
    return 0;
}

